import requests
from bs4 import BeautifulSoup


def get_parser(link):
    return BeautifulSoup(requests.get(link).text, features="lxml")


def ask_for_device(devices):
    message = ""
    message += "The list of devices:\n"
    # range: we need number too
    for i in range(len(devices)):
        message += f"[{i + 1}] {devices[i]}\n"
    print(message)
    answer = input(f"Select a device for the template [1-{len(devices)}]: ")
    result = devices[int(answer) - 1]
    return result


def get_devices_list(parser):
    return [i["title"] for i in parser.select("table.cargoTable td.field_Device a")]


def get_device_page_link(parser, title):
    return "https://wiki.postmarketos.org" + parser.select(f"table.cargoTable td.field_Device a[title='{title}']")[0]["href"]


def check_feature_legitimacy(feature):
    return feature not in ["Mainline", "FOSS bootloader"]


def get_list_of_features(parser):
    return filter(check_feature_legitimacy, [i.get_text() for i in parser.select("tr:has(.feature-yes) th, tr:has(.feature-partial) th")])


def generate_template(device, page, list_of_features):
    return f"""
Generated template:

Device: `{device}` ([wiki]({page}))  
UI: `FIXME`  
Installation method: `FIXME`  
Image (if installed via image): `FIXME.img.xz`

- [ ] Device boots and it's possible to login (password: 147147)

##### Features
{"\n".join([f"- [ ] {i}" for i in list_of_features])}

##### Extra notes:

##### Verdict:
- [ ] Testing complete, :ship: it
"""


def main():
    devices_parser = get_parser("https://wiki.postmarketos.org/wiki/Devices")
    devices = get_devices_list(devices_parser)
    selected_device = ask_for_device(devices)
    device_page = get_device_page_link(devices_parser, selected_device)
    device_page_parser = get_parser(device_page)
    list_of_features = get_list_of_features(device_page_parser)
    template = generate_template(selected_device, device_page, list_of_features)

    print(template)


if __name__ == "__main__":
    main()
